const fs = require('fs');
const express = require('express');
const path = require("path");
let router = express.Router();

function makeDirectory(randomDirectory) {
    let pathDirectory = path.join(__dirname, "../", randomDirectory);

    return new Promise((resolve, reject) => {
        // create randomDirectory
        fs.mkdir(pathDirectory, { recursive: true }, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve("Directory created successfully");
            }
        });
    });
}

function creatEachFile(randomDirectory, fileNames) {
    // creates files
    return new Promise((resolve, reject) => {
        const promises = fileNames.map((file) => {
            const fileName = `./${randomDirectory}/${file}.json`;
            fs.writeFile(fileName, JSON.stringify({ 'name': 'Dadu', 'age': 25 }), (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve("Files created successfully");
                }
            })
        })
        return Promise.all(promises);
    })
}

router.post('/', (req, res, next) => {
    const { randomDirectory, files } = req.body;

    if (!randomDirectory || !files || !Array.isArray(files) || randomDirectory === undefined || files === undefined || typeof randomDirectory !== 'string') {
        console.error("Give key as randomDirectory and files");
        next("Enter keys as randomDirectory and files");
    } else if (randomDirectory.includes('/')) {
        next("Directory  or file should not include /");
    } else {
        makeDirectory(randomDirectory)
            .then((message) => {
                console.log(message);
            })
            .then(() => {
                return creatEachFile(randomDirectory, files);
            })
            .then((message) => {
                console.log(message);
            })
            .then(() => {
                res
                    .status(200)
                    .json({
                        message: "Directory and files created successfully"
                    })
            })
            .catch((error) => {
                console.error(error.message);
                next("Error creating files and directory")
            })
    }
})

module.exports = router;