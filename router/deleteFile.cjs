const fs = require('fs');
const express = require('express');
const path = require('path');
let router = express.Router();

function deleteFiles(randomDirectory, files) {
    let pathDirectory = path.join(__dirname, "../", randomDirectory);

    return new Promise((resolve, reject) => {
        fs.access(pathDirectory, (error) => {
            if (error) {
                console.error(error);
                reject("Directory not found check directory name");
            } else {
                const promises = files.map((file) => {
                    return new Promise((resolve, reject) => {
                        const fileName = `${pathDirectory}/${file}.json`;
                        fs.unlink(fileName, (error) => {
                            if (error) {
                                console.log(`${file} not found`);
                                reject(error);
                            } else {
                                resolve();
                            }
                        });
                    });
                });
                return Promise.all(promises)
                    .then(() => {
                        resolve();
                    })
                    .catch((err) => {
                        reject(err);
                    });
            }
        });
    });
}

router.delete('/', (req, res, next) => {
    const { randomDirectory, files } = req.body;

    if (!files || !Array.isArray(files) || typeof randomDirectory !== 'string' || randomDirectory === undefined || files === undefined) {
        console.error("Give key as randomDirectory and files");
        next("Enter key as randomDirectory and files");
    } else if (randomDirectory.includes('/')) {
        next("Directory or file should not include / ");
    }else {
        deleteFiles(randomDirectory, files)
            .then(() => {
                console.log('Files deleted successful');
                res.status(200)
                    .json({
                        message: "Files is already deleted"
                    })
            })
            .catch((error) => {
                console.error(error);
                if (error.message.includes('ENOENT')){
                    next("Files does not exist");
                } else if (error.message.includes('EACCES')) {
                    next("Permission denied");
                } else {
                    next(error.message);
                } 
            });
    }
});

module.exports = router;

