const express = require('express');
const app = express();
const PORT = process.env.PORT || 6060;

const createFile = require('./router/createFile.cjs');
const deleteFile = require('./router/deleteFile.cjs');

app.use(express.urlencoded({ extended: true }))
app.use(express.json());

app.use('/create-file', createFile);

app.use('/delete-file', deleteFile);

app.use((err, req, res, next) => {
    if (err) {
        res.status(404)
        .json({
            error: err
        })
    } else {
        res.status(404)
        .json({
            error: "Invalid request body or error in url"
        })
    }
    next();
})

app.listen(PORT, () => {
    console.log(`Server started on the port ${PORT}`);
})